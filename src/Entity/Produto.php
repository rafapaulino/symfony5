<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProdutoRepository")
 */
class Produto
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="O nome do produto não pode ser vazio!")
     */
    private $nome;

    /**
     * @var float
     * 
     * @ORM\Column(type="decimal", scale=2)
     * @Assert\NotBlank(message="O preço do produto não pode ser vazio!")
     */
    private $preco;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="A descrição do produto não pode ser vazia!")
     */
    private $descricao;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(string $nome): self
    {
        $this->nome = $nome;

        return $this;
    }

    public function getPreco()
    {
        return intval($this->preco);
    }

    public function setPreco($preco): self
    {
        $this->preco = intval($preco);

        return $this;
    }

    
    /**
     * Get the value of descricao
     */ 
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set the value of descricao
     *
     * @return  self
     */ 
    public function setDescricao($descricao) : self
    {
        $this->descricao = $descricao;

        return $this;
    }
}
