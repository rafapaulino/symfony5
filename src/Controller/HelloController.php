<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use App\Entity\Produto;

class HelloController extends AbstractController
{
    /**
     * @Route("/hello", name="hello")
     */
    public function index()
    {
        /*return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/HelloController.php',
        ]);*/

        $number = random_int(0, 99);

        return new Response(
            '<html><body>Lucky number: '.$number.'</body></html>'
        );
    }

    /**
     * @Route("/hello-name", name="name")
     */
    public function name()
    {
        $name = 'Rafael';
        return $this->render('hello/name.html.twig', [
            'name' => $name,
        ]);
    }

    /**
     * @Route("/hello-ovo", name="ovo")
     */
    public function ovo()
    {
        $name = 'Rafael Lindão';
        return $this->render('hello/ovo.html.twig', [
            'name' => $name,
        ]);
    }

    /**
     * @Route("/hello-produto", name="produto")
     */
    public function produto()
    {
        $em = $this->getDoctrine()->getManager();

        $produto = new Produto();
        $produto->setNome('Notebook');
        $produto->setPreco(1800.00);

        $em->persist($produto);
        $em->flush();

        return new Response("O produto " . $produto->getId() . "foi criado!");

    }

    /**
     * @Route("/hello-form", name="formulario")
     */
    public function form(Request $request)
    {
        $produto = new Produto;

        $form = $this->createFormBuilder($produto)
        ->add('nome', TextType::class)
        ->add('preco', NumberType::class)
        ->add('enviar', SubmitType::class, ['label' => 'Salvar'])
        ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            return new Response("Formulário está ok!");
        }

        return $this->render('hello/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
