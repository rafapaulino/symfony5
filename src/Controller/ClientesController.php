<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Client;
use Symfony\Component\VarDumper\VarDumper;
use App\Entity\Cliente;
use App\Form\ClienteType;

class ClientesController extends AbstractController
{
    /**
     * @Route("/clientes", name="listar_clientes")
     * 
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $clientes = $em->getRepository(Cliente::class)->findAll();

        return $this->render('clientes/index.html.twig', [
            'clientes' => $clientes
        ]);
    }
    /**
     * @Route("/cliente/visualizar/{id}", name="visualizar_cliente")
     * @return array
     */
    public function view(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $cliente = $em->getRepository(Cliente::class)->find($id);
        
        return $this->render('clientes/view.html.twig', [
            'cliente' => $cliente
        ]);
    }
    /**
     * @Route("cliente/cadastrar", name="cadastrar_cliente")
     *
     * @return array
     */
    public function create(Request $request)
    {
        $cliente = new Cliente();
        $form = $this->createForm(ClienteType::class, $cliente);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($cliente);
            $em->flush();

            $this->addFlash('success', "Cliente foi salvo com sucesso!");
            return $this->redirectToRoute('listar_clientes');
        }

        return $this->render('clientes/create.html.twig', [
            'form' => $form->createView()
        ]);
    }
}