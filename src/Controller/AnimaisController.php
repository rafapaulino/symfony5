<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Animal;
use App\Form\AnimalType;

class AnimaisController extends AbstractController
{
    /**
     * @Route("/animais", name="animais")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $animais = $em->getRepository(Animal::class)->findAll();


        return $this->render('animais/index.html.twig', [
            'animais' => $animais
        ]);
    }

    /**
     * @Route("/animais/visualizar/{id}", name="visualizar_animal")
     * @param Animal $animal
     * @return array
     */
    public function view(Animal $animal)
    {
        return $this->render('animais/index.html.twig', [
            'animal' => $animal,
        ]);
    }
    /**
     * @Route("animal/cadastrar", name="cadastrar_animal")
     */
    public function create(Request $request)
    {
        $animal = new Animal();
        $form = $this->createForm(AnimalType::class, $animal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($animal);
            $em->flush();
            $this->addFlash('success', "Animal foi salvo com sucesso!");
            return $this->redirectToRoute('animais');
        }

        return $this->render('animais/create.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
